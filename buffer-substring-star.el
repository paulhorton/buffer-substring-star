;;; buffer-substring-star.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210504
;; Updated: 20210504
;; Version: 0.0
;; Keywords: buffer, string, text properties

;;; Commentary:


;; Wrapper around buffer-substring(-no-properties)

;;; Change Log:

;;; Code:


(defun buffer-substring* (beg end drop-props?)
  "Return buffer-substring with or without properties;
depending on DROP-PROPS?"
  (cl-check-type beg integer-or-marker)
  (cl-check-type end integer-or-marker)
  (if drop-props?
      (buffer-substring-no-properties beg end)
    (buffer-substring beg end)
    ))



(provide 'buffer-substring-star)


;;; buffer-substring-star.el ends here
